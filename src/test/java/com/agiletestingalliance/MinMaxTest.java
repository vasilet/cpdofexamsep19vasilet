package com.agiletestingalliance;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import java.io.*;
import org.junit.Test;
import org.mockito.Mockito;

public class MinMaxTest extends Mockito {

    @Test
    public void maxValueFirstGreaterTest() throws Exception {

        int k= new MinMax().maxValue(10, 2);
		assertEquals("Max First",10,k);
    }
	@Test
    public void maxValueSecondGreaterTest() throws Exception {

        int k= new MinMax().maxValue(2, 10);
		assertEquals("Max First",10,k);
    }
	
	@Test
    public void maxValueEqualsTest() throws Exception {

        int k= new MinMax().maxValue(10, 10);
		assertEquals("Max Any",10,k);
    }
}
